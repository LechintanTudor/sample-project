package com.tora.calculator.atom;

import com.tora.calculator.operand.Number;
import com.tora.calculator.operators.AdditionOperator;
import com.tora.calculator.operators.DivisionOperator;
import com.tora.calculator.operators.MultiplicationOperator;
import com.tora.calculator.operators.SubtractionOperator;

public class Atoms {
    public static Atom fromString(String atom) {
        if (atom.equals("+")) {
            return AdditionOperator.INSTANCE;
        } else if (atom.equals("-")) {
            return SubtractionOperator.INSTANCE;
        } else if (atom.equals("*")) {
            return MultiplicationOperator.INSTANCE;
        } else if (atom.equals("/")) {
            return DivisionOperator.INSTANCE;
        } else {
            return new Number(Integer.parseInt(atom));
        }
    }
}
