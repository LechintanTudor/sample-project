package com.tora.calculator.expression;

import com.tora.calculator.atom.Atom;
import com.tora.calculator.operand.Number;
import com.tora.calculator.operators.Operator;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;

public class Expression {
    private final List<Atom> atoms;

    public static Expression fromInfixAtoms(List<Atom> infixAtoms) {
        if (infixAtoms.size() == 0) {
            throw new RuntimeException("Expressions must not be empty");
        }

        if (infixAtoms.size() % 2 != 1) {
            throw new RuntimeException("Expressions must have an odd number of atoms");
        }

        var atoms = new ArrayList<Atom>();
        var operators = new ArrayDeque<Operator>();

        for (int i = 0; i < infixAtoms.size(); ++i) {
            var atom = infixAtoms.get(i);

            if (i % 2 == 0) {
                if (!(atom instanceof Number)) {
                    throw new RuntimeException("Atoms at even indexes must be operands");
                }

                atoms.add(atom);
            } else {
                if (!(atom instanceof Operator)) {
                    throw new RuntimeException("Atoms at odd indexes must be operators");
                }

                var operator = (Operator) atom;

                while (!operators.isEmpty() && operators.peek().precedence() >= operator.precedence()) {
                    atoms.add(operators.pop());
                }

                operators.push(operator);
            }
        }

        while (!operators.isEmpty()) {
            atoms.add(operators.pop());
        }

        return new Expression(atoms);
    }

    private Expression(List<Atom> atoms) {
        this.atoms = atoms;
    }

    public int evaluate() {
        var atomStack = new ArrayDeque<Atom>();

        for (var atom : atoms) {
            if ((atom instanceof Number)) {
                atomStack.push(atom);
            } else {
                var operator = (Operator) atom;
                var secondOperand = ((Number) atomStack.pop()).getValue();
                var firstOperand = ((Number) atomStack.pop()).getValue();

                var result = operator.evaluate(firstOperand, secondOperand);
                atomStack.push(new Number(result));
            }
        }

        if (atomStack.peek() == null) {
            throw new RuntimeException("Unreachable");
        }

        return ((Number) atomStack.peek()).getValue();
    }

    @Override
    public String toString() {
        var stringBuilder = new StringBuilder();
        stringBuilder.append(atoms.get(0));

        for (int i = 1; i < atoms.size(); ++i) {
            stringBuilder.append(' ');
            stringBuilder.append(atoms.get(i));
        }

        return stringBuilder.toString();
    }
}
