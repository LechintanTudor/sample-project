package com.tora.calculator.operand;

import com.tora.calculator.atom.Atom;

public class Number implements Atom {
    private final int value;

    public Number(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return Integer.toString(value);
    }
}
