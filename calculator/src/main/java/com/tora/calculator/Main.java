package com.tora.calculator;

import com.tora.calculator.atom.Atoms;
import com.tora.calculator.expression.Expression;

import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        System.out.println("Input an infix expression...");

        var scanner = new Scanner(System.in);
        var input = scanner.nextLine();
        var atomsStrings = input.split("\\s+");
        scanner.close();

        var infixAtoms = Arrays.stream(atomsStrings)
                .map(Atoms::fromString)
                .collect(Collectors.toList());

        var expression = Expression.fromInfixAtoms(infixAtoms);
        System.out.printf("\nPostfix expression\n%s\n", expression);

        var result = expression.evaluate();
        System.out.printf("\nResult is %d\n", result);
    }
}
