package com.tora.calculator.operators;

public class MultiplicationOperator implements Operator {
    public static MultiplicationOperator INSTANCE = new MultiplicationOperator();

    private MultiplicationOperator() {
        // Empty
    }

    @Override
    public int precedence() {
        return 2;
    }

    @Override
    public int evaluate(int a, int b) {
        return a * b;
    }

    @Override
    public String toString() {
        return "*";
    }
}
