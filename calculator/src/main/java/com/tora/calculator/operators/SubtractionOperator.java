package com.tora.calculator.operators;

public class SubtractionOperator implements Operator {
    public static SubtractionOperator INSTANCE = new SubtractionOperator();

    private SubtractionOperator() {
        // Empty
    }

    @Override
    public int precedence() {
        return 1;
    }

    @Override
    public int evaluate(int a, int b) {
        return a - b;
    }

    @Override
    public String toString() {
        return "-";
    }
}
