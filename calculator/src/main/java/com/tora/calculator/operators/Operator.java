package com.tora.calculator.operators;

import com.tora.calculator.atom.Atom;

public interface Operator extends Atom {
    int precedence();

    int evaluate(int a, int b);
}
