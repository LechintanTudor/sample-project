package com.tora.calculator.operators;

public class DivisionOperator implements Operator {
    public static DivisionOperator INSTANCE = new DivisionOperator();

    private DivisionOperator() {
        // Empty
    }

    @Override
    public int precedence() {
        return 2;
    }

    @Override
    public int evaluate(int a, int b) {
        return a / b;
    }

    @Override
    public String toString() {
        return "/";
    }
}
