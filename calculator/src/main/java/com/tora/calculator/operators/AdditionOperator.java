package com.tora.calculator.operators;

public class AdditionOperator implements Operator {
    public static AdditionOperator INSTANCE = new AdditionOperator();

    private AdditionOperator() {
        // Empty
    }

    @Override
    public int precedence() {
        return 1;
    }

    @Override
    public int evaluate(int a, int b) {
        return a + b;
    }

    @Override
    public String toString() {
        return "+";
    }
}
