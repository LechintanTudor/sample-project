package com.tora;

import com.tora.calculator.atom.Atoms;
import com.tora.calculator.operand.Number;
import com.tora.calculator.operators.AdditionOperator;
import com.tora.calculator.operators.DivisionOperator;
import com.tora.calculator.operators.MultiplicationOperator;
import com.tora.calculator.operators.SubtractionOperator;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class CalculatorTest {
    @Test
    void additionOperatorParsing() {
        assertSame(Atoms.fromString("+"), AdditionOperator.INSTANCE);
    }

    @Test
    void subtractionOperatorParsing() {
        assertSame(Atoms.fromString("-"), SubtractionOperator.INSTANCE);
    }

    @Test
    void multiplicationOperatorParsing() {
        assertSame(Atoms.fromString("*"), MultiplicationOperator.INSTANCE);
    }

    @Test
    void divisionOperatorParsing() {
        assertSame(Atoms.fromString("/"), DivisionOperator.INSTANCE);
    }

    @Test
    void numberParsing() {
        assertEquals(((Number) Atoms.fromString("10")).getValue(), 10);
        assertEquals(((Number) Atoms.fromString("-10")).getValue(), -10);
        assertThrows(RuntimeException.class, () -> Atoms.fromString("1a0"));
    }
}
