package com.tora;

import com.tora.chat.ChatServer;
import com.tora.console.ConsoleUi;

import java.io.IOException;

public class App {
    public static void main(String[] args) throws IOException, InterruptedException {
        var chatServer = new ChatServer();
        var chatThread = new Thread(() -> {
            try {
                chatServer.start();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
        chatThread.start();

        var consoleUi = new ConsoleUi(chatServer);
        consoleUi.run();

        chatServer.shutdown();
        chatThread.join();
    }
}
