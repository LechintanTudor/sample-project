package com.tora.console;

import java.net.InetSocketAddress;

public class Utils {
    public static String[] splitCommandLine(String commandLine) {
        var words = commandLine.split("\\s+", 2);

        if (words.length == 0) {
            return new String[]{"", ""};
        } else if (words.length == 1) {
            return new String[]{words[0].substring(1).trim(), ""};
        } else {
            return new String[]{words[0].substring(1).trim(), words[1].trim()};
        }
    }

    public static InetSocketAddress parseAddress(String addressString) {
        var words = addressString.trim().split(":", 2);

        if (words.length != 2) {
            throw new RuntimeException("Address string is too short or doesn't contain a colon");
        }

        var address = words[0];
        var port = 0;

        try {
            port = Integer.parseInt(words[1]);
        } catch (NumberFormatException exception) {
            throw new RuntimeException("Failed to parse port");
        }

        return new InetSocketAddress(address, port);
    }

    public static String formatAddress(InetSocketAddress address) {
        return String.format("%s:%d", address.getHostName(), address.getPort());
    }
}
