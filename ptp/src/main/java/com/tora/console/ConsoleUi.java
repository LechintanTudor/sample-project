package com.tora.console;

import com.tora.chat.ChatServer;
import com.tora.chat.MessageType;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class ConsoleUi {
    private final Map<String, Command> commands;
    private final ChatServer chatServer;
    private boolean shouldExit;
    private InetSocketAddress targetAddress;

    public ConsoleUi(ChatServer chatServer) throws IOException {
        this.shouldExit = false;

        commands = new TreeMap<>();
        commands.put("help", new Command(this::cmdShowHelp, "display this message"));
        commands.put("server-info", new Command(this::cmdShowServerInfo, "show the server address"));
        commands.put("exit", new Command(this::cmdExit, "exit the application"));
        commands.put("hi", new Command(this::cmdOpenChat, "start a new chat or resume an existing one"));
        commands.put("chats", new Command(this::cmdListActiveChats, "list active chats"));
        commands.put("check", new Command(this::cmdCheckForNewMessages, "check for new messages in the current chat"));
        commands.put("bye", new Command(this::cmdCloseChat, "stop an active chat"));

        this.chatServer = chatServer;
        this.targetAddress = null;
    }

    public void run() throws InterruptedException, IOException {
        var scanner = new Scanner(System.in);

        var serverAddress = chatServer.getServerAddress();
        System.out.printf("Server running on: %s\n", Utils.formatAddress(serverAddress));

        while (!shouldExit) {
            if (targetAddress != null) {
                System.out.printf("[%s]: ", Utils.formatAddress(targetAddress));
            } else {
                System.out.print("[]: ");
            }
            System.out.flush();

            var commandLine = scanner.nextLine();

            if (commandLine.isBlank()) {
                continue;
            }

            if (commandLine.startsWith("!")) {
                var commandList = Utils.splitCommandLine(commandLine);
                var command = commandList[0];
                var argument = commandList[1];

                if (commands.containsKey(command)) {
                    try {
                        commands.get(command).getCommand().accept(argument);
                    } catch (Exception exception) {
                        System.out.printf("An error occurred: %s\n", exception);
                    }
                } else {
                    System.out.printf("Unknown command: \"%s\"\n", command);
                }
            } else if (targetAddress != null) {
                if (!chatServer.sendMessage(targetAddress, commandLine)) {
                    System.out.println("Failed to send message");
                }
            }

            System.out.println();
        }
    }

    private void cmdShowHelp(String arg) {
        for (var commandEntry : commands.entrySet()) {
            System.out.printf("- %s: %s\n", commandEntry.getKey(), commandEntry.getValue().getDescription());
        }
    }

    private void cmdShowServerInfo(String arg) {
        InetSocketAddress serverAddress;

        try {
            serverAddress = chatServer.getServerAddress();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        System.out.printf("Server running on: %s\n", Utils.formatAddress(serverAddress));
    }

    private void cmdExit(String arg) {
        System.out.println("Goodbye!");
        shouldExit = true;
    }

    private void cmdOpenChat(String arg) {
        var address = Utils.parseAddress(arg);

        try {
            chatServer.openConnection(address);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        targetAddress = address;

        chatServer.forEachNewMessage(address, message -> {
            if (message.getType() == MessageType.RECEIVED) {
                System.out.printf(">>> %s\n", message.getText());
            } else {
                System.out.printf("<<< %s\n", message.getText());
            }
        });
    }

    private void cmdListActiveChats(String arg) {
        System.out.println("[Active Chats]");

        chatServer.forEachActiveChat(activeChat -> {
            try {
                var chat = activeChat.getChat();

                if (chat.getNewMessageCount() > 0) {
                    System.out.printf(
                        "- %s (%d unread message(s))\n",
                        Utils.formatAddress(activeChat.getSocketAddress()),
                        chat.getNewMessageCount()
                    );
                } else {
                    System.out.printf(
                        "- %s\n",
                        Utils.formatAddress(activeChat.getSocketAddress())
                    );
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }

    private void cmdCheckForNewMessages(String arg) {
        if (targetAddress == null) {
            return;
        }
        chatServer.forEachNewMessage(targetAddress, message -> {
            if (message.getType() == MessageType.RECEIVED) {
                System.out.printf(">>> %s\n", message.getText());
            } else {
                System.out.printf("<<< %s\n", message.getText());
            }
        });
    }

    private void cmdCloseChat(String arg) {
        if (arg.isEmpty()) {
            targetAddress = null;
            return;
        }

        var address = Utils.parseAddress(arg);

        try {
            if (chatServer.closeConnection(address)) {
                System.out.printf(
                    "Closed chat with %s\n",
                    Utils.formatAddress(address)
                );
            } else {
                System.out.printf(
                    "No active chat with %s was found\n",
                    Utils.formatAddress(address)
                );
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
