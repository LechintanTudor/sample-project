package com.tora.console;

import java.util.function.Consumer;

public class Command {
    private final Consumer<String> command;
    private final String description;

    public Command(Consumer<String> command, String description) {
        this.command = command;
        this.description = description;
    }

    public Consumer<String> getCommand() {
        return command;
    }

    public String getDescription() {
        return description;
    }
}
