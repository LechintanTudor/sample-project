package com.tora.chat;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;

public class ActiveChat {
    private final SelectionKey selectionKey;
    private final Chat chat;

    public ActiveChat(SelectionKey selectionKey) {
        this.selectionKey = selectionKey;
        this.chat = new Chat();
    }

    SelectionKey getSelectionKey() {
        return selectionKey;
    }

    SocketChannel getSocketChannel() {
        return (SocketChannel) selectionKey.channel();
    }

    public InetSocketAddress getSocketAddress() throws IOException {
        return (InetSocketAddress) getSocketChannel().getRemoteAddress();
    }

    public Chat getChat() {
        return chat;
    }
}
