package com.tora.chat;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

public class ChatServer {
    private final AtomicBoolean shouldExit;
    private final Selector selector;
    private final ServerSocketChannel listeningSocket;
    private final Map<InetSocketAddress, ActiveChat> activeChats;
    private final ByteBuffer byteBuffer;

    public ChatServer() throws IOException {
        shouldExit = new AtomicBoolean(false);
        selector = Selector.open();
        listeningSocket = ServerSocketChannel.open();
        activeChats = new HashMap<>();
        byteBuffer = ByteBuffer.allocate(1024);
    }

    public void start() throws IOException {
        listeningSocket.bind(new InetSocketAddress("localhost", 0));
        listeningSocket.configureBlocking(false);

        var listeningSocketSelectionKey = listeningSocket.register(selector, SelectionKey.OP_ACCEPT);

        for (; ; ) {
            selector.select();

            if (shouldExit.get()) {
                break;
            }

            for (var iter = selector.selectedKeys().iterator(); iter.hasNext(); iter.remove()) {
                var selectionKey = iter.next();

                if (selectionKey.isAcceptable()) {
                    acceptConnection();
                } else if (selectionKey.isReadable()) {
                    receiveMessage(selectionKey);
                }
            }
        }

        if (listeningSocketSelectionKey.isValid()) {
            listeningSocketSelectionKey.cancel();
            listeningSocketSelectionKey.channel().close();
        }

        synchronized (activeChats) {
            activeChats
                .values()
                .stream()
                .map(ActiveChat::getSelectionKey)
                .forEach(selectionKey -> {
                    if (selectionKey.isValid()) {
                        selectionKey.cancel();

                        try {
                            selectionKey.channel().close();
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    }
                });
        }

        selector.close();
    }

    public void shutdown() {
        shouldExit.set(true);
        selector.wakeup();
    }

    public InetSocketAddress getServerAddress() throws IOException {
        return (InetSocketAddress) listeningSocket.getLocalAddress();
    }

    public boolean openConnection(InetSocketAddress address) throws IOException {
        synchronized (activeChats) {
            if (activeChats.containsKey(address)) {
                return false;
            }

            var clientSocket = SocketChannel.open(address);
            clientSocket.configureBlocking(false);

            var selectionKey = clientSocket.register(selector, SelectionKey.OP_READ);
            activeChats.put(address, new ActiveChat(selectionKey));

            return true;
        }
    }

    public boolean sendMessage(InetSocketAddress address, String messageText) throws IOException {
        synchronized (activeChats) {
            var activeChat = activeChats.get(address);

            if (activeChat == null) {
                return false;
            }

            var clientSocket = activeChat.getSocketChannel();
            clientSocket.write(ByteBuffer.wrap(messageText.getBytes()));
            activeChat.getChat().onMessageSent(messageText);

            return true;
        }
    }

    public void forEachActiveChat(Consumer<ActiveChat> chatConsumer) {
        synchronized (activeChats) {
            activeChats.values().forEach(chatConsumer);
        }
    }

    public boolean forEachNewMessage(InetSocketAddress address, Consumer<Message> messageConsumer) {
        synchronized (activeChats) {
            var activeChat = activeChats.get(address);

            if (activeChat == null) {
                return false;
            }

            activeChat
                .getChat()
                .consumeNewMessages()
                .forEach(messageConsumer);

            return true;
        }
    }

    public boolean closeConnection(InetSocketAddress address) throws IOException {
        SelectionKey selectionKey;

        synchronized (activeChats) {
            var activeChat = activeChats.get(address);

            if (address == null) {
                return false;
            }

            selectionKey = activeChat.getSelectionKey();
            activeChats.remove(address);
        }

        if (selectionKey.isValid()) {
            selectionKey.cancel();
            selectionKey.channel().close();
        }

        return true;
    }

    private void acceptConnection() throws IOException {
        var client = listeningSocket.accept();
        client.configureBlocking(false);

        var selectionKey = client.register(selector, SelectionKey.OP_READ);
        var activeChat = new ActiveChat(selectionKey);
        var address = activeChat.getSocketAddress();

        synchronized (activeChats) {
            activeChats.put(address, new ActiveChat(selectionKey));
        }
    }

    private void receiveMessage(SelectionKey selectionKey) throws IOException {
        var clientSocket = (SocketChannel) selectionKey.channel();
        var clientAddress = (InetSocketAddress) clientSocket.getRemoteAddress();

        byteBuffer.clear();
        var bytesRead = clientSocket.read(byteBuffer);

        if (bytesRead == -1) {
            selectionKey.cancel();
            clientSocket.close();

            synchronized (activeChats) {
                activeChats.remove(clientAddress);
            }
        } else if (bytesRead > 0) {
            var messageText = new String(Arrays.copyOf(byteBuffer.array(), bytesRead));

            synchronized (activeChats) {
                activeChats
                    .values()
                    .stream()
                    .filter(chat -> chat.getSelectionKey().equals(selectionKey))
                    .findFirst()
                    .ifPresent(chat -> chat.getChat().onMessageReceived(messageText));
            }
        }
    }
}
