package com.tora.chat;

import java.util.ArrayList;
import java.util.List;

public class Chat {
    private final List<Message> messages;
    private int lastCheckedMessageIndex;

    public Chat() {
        this.messages = new ArrayList<>();
        this.lastCheckedMessageIndex = 0;
    }

    void onMessageReceived(String messageText) {
        messages.add(Message.received(messageText));
    }

    void onMessageSent(String messageText) {
        messages.add(Message.sent(messageText));
    }

    public int getNewMessageCount() {
        return messages.size() - lastCheckedMessageIndex;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public List<Message> consumeNewMessages() {
        var newMessagesStartIndex = lastCheckedMessageIndex;
        var newMessageList = messages.subList(newMessagesStartIndex, messages.size());
        lastCheckedMessageIndex = messages.size();
        return newMessageList;
    }
}
