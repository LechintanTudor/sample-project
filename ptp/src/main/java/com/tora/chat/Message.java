package com.tora.chat;

public class Message {
    private final String text;
    private final MessageType type;

    public Message(String text, MessageType type) {
        this.text = text;
        this.type = type;
    }

    public static Message sent(String text) {
        return new Message(text, MessageType.SENT);
    }

    public static Message received(String text) {
        return new Message(text, MessageType.RECEIVED);
    }

    public String getText() {
        return text;
    }

    public MessageType getType() {
        return type;
    }
}
