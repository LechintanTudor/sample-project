package com.tora.chat;

public enum MessageType {
    SENT,
    RECEIVED,
}
