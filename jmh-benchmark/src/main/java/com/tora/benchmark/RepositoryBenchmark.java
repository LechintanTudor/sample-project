package com.tora.benchmark;

import com.tora.model.Order;
import com.tora.repository.*;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;

import java.util.concurrent.TimeUnit;

@Fork(value = 1)
@Warmup(iterations = 3, time = 3)
@Measurement(iterations = 5, time = 3)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
public class RepositoryBenchmark {
    private static final int FIRST_DEFAULT_ID = 1;
    private static final int LAST_DEFAULT_ID = 1000;
    private static final int FIRST_ADDABLE_ID = 1001;
    private static final int LAST_ADDABLE_ID = 2000;

    @Benchmark
    public void benchmarkAdd(BenchmarkState state, Blackhole blackhole) {
        for (int i = FIRST_ADDABLE_ID; i <= LAST_ADDABLE_ID; ++i) {
            state.orderRepository.add(new Order(i, 1, 1));
            blackhole.consume(state.orderRepository);
        }
    }

    @Benchmark
    public void benchmarkContains(BenchmarkState state, Blackhole blackhole) {
        for (int i = FIRST_DEFAULT_ID; i <= LAST_DEFAULT_ID; ++i) {
            blackhole.consume(state.orderRepository.contains(new Order(i, 1, 1)));
        }
    }

    @Benchmark
    public void benchmarkRemove(BenchmarkState state, Blackhole blackhole) {
        for (int i = FIRST_DEFAULT_ID; i <= LAST_DEFAULT_ID; ++i) {
            state.orderRepository.remove(new Order(i, 1, 1));
            blackhole.consume(state.orderRepository);
        }
    }

    @State(Scope.Benchmark)
    public static class BenchmarkState {
        @Param({"array-list", "array-set", "concurrent-hash-map", "hash-set", "tree-set", "unified-set"})
        public static String repositoryType;

        public InMemoryRepository<Order> orderRepository;

        @Setup(Level.Trial)
        public void setUpTrial() {
            switch (repositoryType) {
                case "array-list":
                    orderRepository = new ArrayListBasedRepository<>();
                    break;

                case "array-set":
                    orderRepository = new ArraySetBasedRepository<>();
                    break;

                case "concurrent-hash-map":
                    orderRepository = new ConcurrentHashMapBasedRepository<>();
                    break;

                case "hash-set":
                    orderRepository = new HashSetBasedRepository<>();
                    break;

                case "tree-set":
                    orderRepository = new TreeSetBasedRepository<>();
                    break;

                case "unified-set":
                    orderRepository = new UnifiedSetBasedRepository<>();
                    break;

                default:
                    throw new IllegalArgumentException("Invalid repository type");
            }
        }

        @Setup(Level.Iteration)
        public void setUpIteration() {
            for (int i = FIRST_DEFAULT_ID; i <= LAST_DEFAULT_ID; ++i) {
                orderRepository.add(new Order(i, 1, 1));
            }
        }

        @TearDown(Level.Iteration)
        public void tearDownIteration() {
            for (int i = FIRST_DEFAULT_ID; i <= LAST_ADDABLE_ID; ++i) {
                orderRepository.remove(new Order(i, 1, 1));
            }
        }
    }
}
