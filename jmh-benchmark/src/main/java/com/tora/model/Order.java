package com.tora.model;

public class Order implements Comparable<Order> {
    private final int id;
    private final int price;
    private final int quantity;

    public Order(int id, int price, int quantity) {
        this.id = id;
        this.price = price;
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Order) {
            return id == ((Order) obj).id;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return Integer.hashCode(id);
    }

    @Override
    public int compareTo(Order order) {
        return Integer.compare(id, order.id);
    }

    @Override
    public String toString() {
        return String.format("{ id: %d, price: %d, quantity: %d }", id, price, quantity);
    }

    public int getId() {
        return id;
    }

    public int getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }
}
