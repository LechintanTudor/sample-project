package com.tora.repository;

import java.util.HashSet;
import java.util.Set;

public class HashSetBasedRepository<T> implements InMemoryRepository<T> {
    private final Set<T> elements;

    public HashSetBasedRepository() {
        elements = new HashSet<>();
    }

    @Override
    public void add(T element) {
        elements.add(element);
    }

    @Override
    public boolean contains(T element) {
        return elements.contains(element);
    }

    @Override
    public void remove(T element) {
        elements.remove(element);
    }
}
