package com.tora.repository;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapBasedRepository<T> implements InMemoryRepository<T> {
    private final Map<T, Object> elements;

    public ConcurrentHashMapBasedRepository() {
        this.elements = new ConcurrentHashMap<>();
    }

    @Override
    public void add(T element) {
        elements.put(element, null);
    }

    @Override
    public boolean contains(T element) {
        return elements.containsKey(element);
    }

    @Override
    public void remove(T element) {
        elements.remove(element);
    }
}
