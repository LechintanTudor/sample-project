package com.tora.repository;

import it.unimi.dsi.fastutil.objects.ObjectArraySet;

import java.util.Set;

public class ArraySetBasedRepository<T> implements InMemoryRepository<T> {
    private final Set<T> elements;

    public ArraySetBasedRepository() {
        elements = new ObjectArraySet<>();
    }

    @Override
    public void add(T element) {
        elements.add(element);
    }

    @Override
    public boolean contains(T element) {
        return elements.contains(element);
    }

    @Override
    public void remove(T element) {
        elements.remove(element);
    }
}
