package com.tora.repository;

import java.util.Set;
import java.util.TreeSet;

public class TreeSetBasedRepository<T extends Comparable<T>> implements InMemoryRepository<T> {
    private final Set<T> elements;

    public TreeSetBasedRepository() {
        elements = new TreeSet<>();
    }

    @Override
    public void add(T element) {
        elements.add(element);
    }

    @Override
    public boolean contains(T element) {
        return elements.contains(element);
    }

    @Override
    public void remove(T element) {
        elements.remove(element);
    }
}
