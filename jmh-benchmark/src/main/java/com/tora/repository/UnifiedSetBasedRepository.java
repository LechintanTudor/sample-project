package com.tora.repository;

import org.eclipse.collections.impl.set.mutable.UnifiedSet;

import java.util.Set;

public class UnifiedSetBasedRepository<T> implements InMemoryRepository<T> {
    private final Set<T> elements;

    public UnifiedSetBasedRepository() {
        elements = new UnifiedSet<>();
    }

    @Override
    public void add(T element) {
        elements.add(element);
    }

    @Override
    public boolean contains(T element) {
        return elements.contains(element);
    }

    @Override
    public void remove(T element) {
        elements.remove(element);
    }
}
