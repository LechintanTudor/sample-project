package com.tora.repository;

import java.util.ArrayList;
import java.util.List;

public class ArrayListBasedRepository<T> implements InMemoryRepository<T> {
    private final List<T> elements;

    public ArrayListBasedRepository() {
        elements = new ArrayList<>();
    }

    @Override
    public void add(T element) {
        if (!elements.contains(element)) {
            elements.add(element);
        }
    }

    @Override
    public boolean contains(T element) {
        return elements.contains(element);
    }

    @Override
    public void remove(T element) {
        int index = elements.indexOf(element);

        if (index != -1) {
            int lastIndex = elements.size() - 1;
            elements.set(index, elements.get(lastIndex));
            elements.remove(lastIndex);
        }
    }
}
