package com.tora;

import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.Random;
import java.util.function.IntSupplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class BigDecimalFileDemo {
    private static final String BIG_DECIMAL_FILE_NAME = "big-decimals.data";
    private static final int BIG_DECIMAL_COUNT = 1013;

    public static void main(String[] args) throws IOException {
        generateBigDecimalsFile();
        computeSumAndAverageFromBigDecimalsFile();
        computeTop10PercentFromBigDecimalsFile();
        deleteBigDecimalsFile();
    }

    private static void generateBigDecimalsFile() throws IOException {
        var random = new Random();
        IntSupplier generateDigitCount = () -> random.nextInt(11) + 10;
        IntSupplier generateFirstDigit = () -> random.nextInt(9) + 1;
        IntSupplier generateAnyDigit = () -> random.nextInt(10);

        try (
                var baseOutputStream = new FileOutputStream(BIG_DECIMAL_FILE_NAME);
                var objectOutputStream = new ObjectOutputStream(baseOutputStream)
        ) {

            for (var i = 0; i < BIG_DECIMAL_COUNT; ++i) {
                var leftDigitCount = generateDigitCount.getAsInt();
                var rightDigitCount = generateDigitCount.getAsInt();
                var bigDecimalStringBuilder = new StringBuilder();

                bigDecimalStringBuilder.append(generateFirstDigit.getAsInt());

                for (var j = 0; j < leftDigitCount; ++j) {
                    bigDecimalStringBuilder.append(generateAnyDigit.getAsInt());
                }

                bigDecimalStringBuilder.append('.');

                for (var j = 0; j < rightDigitCount; ++j) {
                    bigDecimalStringBuilder.append(generateAnyDigit.getAsInt());
                }

                var bigDecimal = new BigDecimal(bigDecimalStringBuilder.toString());
                BigDecimalSerializer.serializeBigDecimal(objectOutputStream, bigDecimal);
            }
        }
    }

    private static void computeSumAndAverageFromBigDecimalsFile() throws IOException {
        try (
            var baseInputStream = new FileInputStream(BIG_DECIMAL_FILE_NAME);
            var objectInputStream = new ObjectInputStream(baseInputStream)
        ) {
            var sum = IntStream.range(0, BIG_DECIMAL_COUNT)
                .mapToObj(i -> {
                    try {
                        return BigDecimalSerializer.deserializeBigDecimal(objectInputStream);
                    } catch (IOException error) {
                        throw new RuntimeException(error);
                    }
                })
                .reduce(BigDecimal.ZERO, BigDecimal::add);

            System.out.printf("Sum is %s\n", sum);
            System.out.printf("Average is %s\n", sum.divide(new BigDecimal(BIG_DECIMAL_COUNT), RoundingMode.CEILING));
        }
    }

    private static void computeTop10PercentFromBigDecimalsFile() throws IOException {
        try (
            var baseInputStream = new FileInputStream(BIG_DECIMAL_FILE_NAME);
            var objectInputStream = new ObjectInputStream(baseInputStream)
        ) {
            var sortedNumbers = IntStream.range(0, BIG_DECIMAL_COUNT)
                .mapToObj(i -> {
                    try {
                        return BigDecimalSerializer.deserializeBigDecimal(objectInputStream);
                    } catch (IOException error) {
                        throw new RuntimeException(error);
                    }
                })
                .sorted(Comparator.reverseOrder())
                .limit((long) (BIG_DECIMAL_COUNT * 0.1))
                .collect(Collectors.toList());

            System.out.println("\nLargest numbers are:");

            for (var number : sortedNumbers) {
                System.out.println(number);
            }
        }
    }

    private static void deleteBigDecimalsFile() {
        var file = new File(BIG_DECIMAL_FILE_NAME);
        System.out.println();

        if (file.delete()) {
            System.out.println("File deleted successfully");
        } else {
            System.out.println("Failed to delete file");
        }
    }
}
