package com.tora;

import org.junit.jupiter.api.Test;

import java.io.*;
import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BigDecimalTest {
    @Test
    void testSerializeAndDeserialize() throws IOException {
        var original = new BigDecimal("123456789123456789.123456789123456789");
        var baseOutputStream = new ByteArrayOutputStream();
        var objectOutputStream = new ObjectOutputStream(baseOutputStream);
        BigDecimalSerializer.serializeBigDecimal(objectOutputStream, original);
        objectOutputStream.close();

        var baseInputStream = new ByteArrayInputStream(baseOutputStream.toByteArray());
        var objectInputStream = new ObjectInputStream(baseInputStream);
        var deserialized = BigDecimalSerializer.deserializeBigDecimal(objectInputStream);
        objectInputStream.close();

        assertEquals(original, deserialized);
    }
}